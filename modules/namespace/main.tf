locals {
  original_tags    = join(var.delimiter, concat(list(var.solution, var.supporters)))
  transformed_tags = var.convert_case ? lower(local.original_tags) : local.original_tags
}

locals {
  id = var.enabled ? local.transformed_tags : ""

  solution   = var.enabled ? (var.convert_case ? lower(format("%v", var.solution)) : format("%v", var.solution)) : ""
  supporters = var.enabled ? (var.convert_case ? lower(format("%v", var.supporters)) : format("%v", var.supporters)) : ""

  tags = merge(
    {
      "solution"   = local.solution
      "supporters" = local.supporters
    },
    var.tags
  )
}
