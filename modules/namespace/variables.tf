variable "tags" {
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit','XYZ')`"
}

variable "supporters" {
  type        = string
  default     = ""
  description = "Namespace Supports"
}

variable "solution" {
  type        = string
  default     = ""
  description = "Solution String for this namespace, like technical"
}


variable "enabled" {
  type        = bool
  default     = true
  description = "Set to false to prevent the module from creating any resources"
}

variable "convert_case" {
  type        = bool
  default     = true
  description = "Convert fields to lower case"
}
variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `stage`, `name` and `attributes`"
}
