
module "namespace" {
  source     = "../../modules/namespace"
  supporters = "cluster-admins"
  solution   = "monitoring"
}

output "namespace" {
  value = module.namespace.tags
}
