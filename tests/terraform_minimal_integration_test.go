package test

import (
    "testing"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestTerraformMinimalAccExample(t *testing.T) {
	terraformOptions := &terraform.Options{
		TerraformDir: "../examples/minimal",
	}

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)


    namespaceLabels := terraform.OutputMap(t, terraformOptions, "namespace")

    assert.Equal(t, "monitoring", namespaceLabels["solution"],    "The two words should be the same.")
    assert.Equal(t,"cluster-admins", namespaceLabels["supporters"] , "The two words should be the same.")
}

